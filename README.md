PPPL Dashboard
==============

An up-to-date, chewable panel of the numbers that matter and the words that resonate.

Installation
------------

1. [Install Ruby](http://www.ruby-lang.org/en/downloads/).
2. Clone this repository: `git clone https://casey_chow@bitbucket.org/casey_chow/pppl-dashboard.git`
3. `cd pppl-dashboard`
4. `bundle install`
5. `dashing start`

And your beautiful dashboard should be...almost ready.

### config.yml

PPPL Dashboard keeps its configuration in a `config.yml` in the root directory. Here are the fields you can add:

    :::yaml
    ---
    twitter:
      username:
      consumer_key:
      consumer_secret:
      oauth_token:
      oauth_secret:
      mentions_query:
      profile_query:
    pagerank:
      url:
    rss:
      site_news:
      blog:
    facebook:
      username:
    flickr:
      user:
      api_key:
      api_seret:
    google_analytics:
      service_account_email:
      profile_id:
      keyfile_path:
      key_secret:

 The application will automatically load these configuration options into the correct jobs and widgets.


License  
========

 This package isn't really meant for distribution, so please tread carefully. Basically, the two conditions of using this:

 1. Don't sue me or the Princeton Plasma Physics Laboratory.
 2. If you have to sue me, at least send me a letter first.