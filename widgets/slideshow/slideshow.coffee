class Dashing.Slideshow extends Dashing.Widget

  ready: ->
    # This is fired when the widget is done being rendered
    @currentIndex = 0
    @imageElem = $(@node).find('.image')
    @nextImage()
    @startCarousel()

  onData: (data) ->
    @currentIndex = 0

  startCarousel: ->
    setInterval(@nextImage, 5000)

  nextImage: =>
    images = @get('images')
    if images
      @imageElem.fadeOut =>
        @currentIndex = Math.floor(Math.random() * images.length)
        @set 'current_image', images[@currentIndex]
        @imageElem.fadeIn()