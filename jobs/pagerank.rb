require 'page_rankr'

cfg = YAML.load_file('./config.yml')['pagerank']
ranks_ct = { value: 0 }

SCHEDULER.every '10m', :first_in => 0 do |job|
  puts "#{Time.now}: loading pagerank"
  ranks     = PageRankr.ranks(cfg['url'])[:google]
  indexes   = PageRankr.indexes(cfg['url'])[:google]

  # send_event('rankings',  items:    ranks_ct.values[1..-1])
  send_event('pagerank',      value:  ranks,   current: ranks)
  send_event('indexed_pages', value:  indexes, current: indexes)
end