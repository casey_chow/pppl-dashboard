require 'twitter'

cfg = YAML.load_file('./config.yml')['twitter']

#### Get your twitter keys & secrets:
#### https://dev.twitter.com/docs/auth/tokens-devtwittercom
Twitter.configure do |config|
  config.consumer_key = cfg['consumer_key']
  config.consumer_secret = cfg['consumer_secret']
  config.oauth_token = cfg['oauth_token']
  config.oauth_token_secret = cfg['oauth_secret']
end

SCHEDULER.every '5m', :first_in => 0 do |job|
  puts "#{Time.now}: reloading twitter"
  begin
    mentions = Twitter.search(cfg['mentions_query']).results
    profile = Twitter.search(cfg['profile_query']).results

    send_event('twitter_mentions',  comments: map_tweets(mentions))          if mentions
    send_event('twitter_profile',   comments: map_tweets(profile))           if profile
  rescue Twitter::Error
    puts "\e[33mFor the twitter widget to work, you need to put in your twitter API keys in the jobs/twitter.rb file.\e[0m"
  end
end

# https://github.com/foobugs/foobugs-dashboard/blob/master/jobs/twitter_user.rb
SCHEDULER.every '5m', :first_in => 0 do |job|
  http = Net::HTTP.new("twitter.com", Net::HTTP.https_default_port())
  http.use_ssl = true
  response = http.request(Net::HTTP::Get.new("/#{cfg['username']}"))
  if response.code != "200"
    puts "twitter communication error (status-code: #{response.code})\n#{response.body}"
  else
    tweets = /profile["']>[\n\t\s]*<strong>([\d.,]+)/.match(response.body)[1].delete('.,').to_i
    followers = /followers["']>[\n\t\s]*<strong>([\d.,]+)/.match(response.body)[1].delete('.,').to_i
    
    send_event('twitter_tweets',    value:  tweets,    current: tweets)
    send_event('twitter_followers', value:  followers, current: followers)
  end
end

def map_tweets(tweets)
  tweets.map do |tweet|
    { name: tweet.user.name, body: tweet.text, avatar: tweet.user.profile_image_url_https }
  end
end