require 'google/api_client'
require 'date'

cfg = YAML.load_file('./config.yml')['google_analytics']

# Update these to match your own apps credentials
service_account_email   = cfg['service_account_email'] # Email of service account
key_file                = cfg['keyfile_path'] # File containing your private key
key_secret              = cfg['key_secret'] # Password to unlock private key
profileID               = cfg['profile_id'] # Analytics profile ID.

# Get the Google API client
client = Google::APIClient.new(:application_name => 'PPPL Dashboard', :application_version => '0.01')

# Load your credentials for the service account
key = Google::APIClient::KeyUtils.load_from_pkcs12(key_file, key_secret)
client.authorization = Signet::OAuth2::Client.new(
  :token_credential_uri => 'https://accounts.google.com/o/oauth2/token',
  :audience => 'https://accounts.google.com/o/oauth2/token',
  :scope => 'https://www.googleapis.com/auth/analytics.readonly',
  :issuer => service_account_email,
  :signing_key => key)

# Start the scheduler
SCHEDULER.every '1m', :first_in => 0 do
  puts "#{Time.now}: pulling google analytics"

  # Request a token for our service account
  client.authorization.fetch_access_token!

  # Get the analytics API
  analytics = client.discovered_api('analytics','v3')

  # Start and end dates
  startDate = DateTime.now.strftime("%Y-%m-01") # first day of current month
  endDate = DateTime.now.strftime("%Y-%m-%d")  # now

  # Execute the query
  visitCount = client.execute(:api_method => analytics.data.ga.get, :parameters => { 
    'ids' => "ga:" + profileID, 
    'start-date' => startDate,
    'end-date' => endDate,
    # 'dimensions' => "ga:month",
    'metrics' => "ga:visitors",
    # 'sort' => "ga:month" 
  })

  # Update the dashboard
  send_event('visitor_count',   { current: visitCount.data.rows[0][0] })
end