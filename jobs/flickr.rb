require 'flickraw'
cfg = YAML.load_file('./config.yml')['flickr']

FlickRaw.api_key = cfg['api_key']
FlickRaw.shared_secret = cfg['api_secret']

SCHEDULER.every '15m', :first_in => 0 do |job|
  begin
    list = flickr.photos.search :user_id => cfg['user']
  rescue
    puts 'flickr photos could not be found'
  else
    converted_list = list.to_a.map { |photo| FlickRaw.url_z(photo) }

    send_event 'flickr', images: converted_list
  end
end